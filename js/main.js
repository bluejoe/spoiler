"use strict";

fetch('https://spreadsheets.google.com/feeds/cells/1rXb8VHj6EkrFqXas-VhJs13GcRp5cI20ZQFr-8riliE/1/public/full?alt=json')
  .then(response => response.json())
  .then(data => easyImages = data.feed.entry);

fetch('https://spreadsheets.google.com/feeds/cells/1rXb8VHj6EkrFqXas-VhJs13GcRp5cI20ZQFr-8riliE/2/public/full?alt=json')
  .then(response => response.json())
  .then(data => normalImages = data.feed.entry);

fetch('https://spreadsheets.google.com/feeds/cells/1rXb8VHj6EkrFqXas-VhJs13GcRp5cI20ZQFr-8riliE/3/public/full?alt=json')
  .then(response => response.json())
  .then(data => hardImages = data.feed.entry);

var imagebox = document.getElementById('movie_scene');
var titlebox = document.getElementById('mtitle');
var buttonbox = document.getElementById('button_holder');
var buttonbox = document.getElementById('button_holder');

let easyImages = [];
let normalImages = [];
let hardImages = [];
let theImages=[];

function display_random_image(x){
  if (x == "easy") {
    theImages = easyImages;
  } else if (x == "normal") {
    theImages = normalImages;
  } else if (x == "hard") {
    theImages = hardImages;
  }
 
  // create random image number
  let imn = null;
  function getRandomInt(min,max){
    imn = Math.floor(Math.random() * (max - min + 1)) + min;
    if (imn % 2) {
      imn--;
    } 
    return theImages[imn];
  }

  var newImage = getRandomInt(0, theImages.length - 1);

  // remove the previous images
  titlebox.style.visibility = "hidden";
  titlebox.style.opacity = "0";
  titlebox.innerHTML = theImages[imn].content.$t;
  imagebox.style.backgroundImage = "url("+theImages[imn + 1].content.$t+")";
  buttonbox.className = 'button_holder--active';
  timer(60);
    
}

function display_name(){
  mtitle.style.visibility = "visible";
  mtitle.style.opacity = "1";
  buttonbox.className = 'button_holder';
  clearInterval(secInterval);
}

function limer(x) {
  var remainingTime = x;
  var elem = document.getElementById('timer');
  var timer = setInterval(countdown, 1000); //set the countdown to every second
  function countdown() {
    if (remainingTime == -1) {
      clearTimeout(timer);
      doSomething();
    } else {
      elem.innerHTML = remainingTime ;
      remainingTime--; //we subtract the second each iteration
    }
  }
}


// document.getElementById("button_go").addEventListener("click", timer);
let topBar = document.getElementById('header');
let progressBar = document.getElementById('progressbar');
let progressBar_spoiler = document.getElementById('spoiler');
let progressBar_timer = document.getElementById('timer');


//Setup Global Interval
var secInterval;
function timer(maxSec) {
  let currentSec = maxSec;
  //Setup tick
  secInterval = window.setInterval(function () {
    updateSec(currentSec, maxSec);
    currentSec--;
    //When timer runs out, stop timer.
    if (currentSec < 0) {
      clearInterval(secInterval);
    }
  }, 1000);
}

function zzupdateSec(sec, max) {
  if (sec < 10) {
    document.getElementById("timer").innerHTML = `&nbsp${sec}`;
    topBar.style.background = "linear-gradient(90deg, #ea4335 0%, #ea4335 " + (sec / max) * 100 + "%, #f1f1f1 0%, #f1f1f1 100%)";
  } else {
    document.getElementById("timer").innerHTML = sec;
    topBar.style.background = "linear-gradient(90deg, #202124 0%, #202124 " + (sec / max) * 100 + "%, #f1f1f1 0%, #f1f1f1 100%)";
  }
}

function updateSec(sec, max) {
  // if(sec > 56){
  //   progressBar_timer.style.color = "#f1f1f1";
  // } else {
  //   progressBar_timer.style.color = "#202124";
  // }

  if (sec < 10) {
    document.getElementById("timer").innerHTML = `&nbsp${sec}`;
    progressBar.style.width = (sec / max) * 100 + "%";
    progressBar.style.background = "#ea4335";
    progressBar_spoiler.style.color = "#202124";

  } else {
    document.getElementById("timer").innerHTML = sec;
    progressBar.style.width = (sec / max) * 100 + "%";
    progressBar.style.background = "#202124";
    progressBar_spoiler.style.color = "#f1f1f1";
  }
}
